import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstArenaFighter = createArenaFighter(
    firstFighter
  );
  const secondArenaFighter = createArenaFighter(
    secondFighter
  );


  const pressedKeys = new Map();

  document.addEventListener('keydown', (e) => {

    pressedKeys.set(e.code, true);

    processFightAction(firstArenaFighter, secondArenaFighter, pressedKeys, e.code);

    if (firstArenaFighter.currentHealth <= 0) {
      resolve(secondFighter);
    } else if (secondArenaFighter.currentHealth <= 0) {
      resolve(firstFighter);
    }
  });

  document.addEventListener('keyup', (e) => {
    if (e.code === controls.PlayerOneBlock) {
      firstArenaFighter.setIsBlocking(false);
    }
    if (e.code === controls.PlayerTwoBlock) {
      secondArenaFighter.setIsBlocking(false);
    }
    pressedKeys.delete(e.code);
  });
});
}

function createArenaFighter(fighter) {

return {
  ...fighter,
  currentHealth: fighter.health,
  isCanDoCrit:true,
  isBlocking: false,
  timerId: null,
  receiveDamage(value) {
    this.currentHealth -= value;
  },
  setIsBlocking(value) {
    this.isBlocking = value;
  },
  doAttack(defender, damage) {
    defender.receiveDamage(damage);
  },
  doCritAttack(defender) {
    if (!this.isCanDoCrit) return;
    defender.receiveDamage(this.attack * 2);
  },
  restartCritPoints() {
this.isCanDoCrit=false
    this.timerId = setInterval(() => {

      this.isCanDoCrit=true;

    }, 10000);//10 secs
  },
};
}

function processFightAction(firstFighter, secondFighter, keyMap, currentCode) {
if (currentCode === controls.PlayerOneBlock) {
  firstFighter.setIsBlocking(true);
}
if (currentCode === controls.PlayerTwoBlock) {
  secondFighter.setIsBlocking(true);
}
if (currentCode === controls.PlayerOneAttack) {
  applyFighterAttack(firstFighter, secondFighter, keyMap);
  return;
}
if (currentCode === controls.PlayerTwoAttack) {
  applyFighterAttack(secondFighter, firstFighter, keyMap);
  return;
}
if (controls.PlayerOneCriticalHitCombination.every(code => keyMap.has(code))) {
  firstFighter.doCritAttack(secondFighter);
  return;
}
if (controls.PlayerTwoCriticalHitCombination.every(code => keyMap.has(code))) {
  secondFighter.doCritAttack(firstFighter);
}
}

function applyFighterAttack(attacker, defender) {
if (attacker.isBlocking) {
  return;
}

if (defender.isBlocking) {
  attacker.doAttack(defender, 0);
  return;
}

  attacker.doAttack(defender, getDamage(attacker, defender));
}

export function getDamage(attacker, defender) {
  const damage =getHitPower(attacker)-getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const luck = (Math.random() * (2 - 1) + 1);
  return fighter.attack*luck;
}

export function getBlockPower(fighter) {
  const luck = (Math.random() * (2 - 1) + 1);
 return fighter.defense*luck;
}
